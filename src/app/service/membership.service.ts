import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MembershipModel} from '../model/membership.model';

@Injectable({
  providedIn: 'root'
})

export class MembershipService {
  private API_URL = '/chat/api/v1/';
  private CHATS = 'chats/';
  private MEMBERSHIPS = '/memberships/';
  private USERS = 'users/';

  constructor(private http: HttpClient) {
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  findByChatId(chatId: number): Observable<MembershipModel[]> {
    return this.http.get<MembershipModel[]>(this.API_URL + this.CHATS + chatId + this.MEMBERSHIPS, this.httpOptions);
  }

  findChatMembershipOfCurrentUser(chatId: number, currentUserLogin: string): Observable<MembershipModel> {
    return this.http.get<MembershipModel>(this.API_URL + this.CHATS + chatId + '/' + this.USERS + currentUserLogin, this.httpOptions);
  }

  addMembership(chatId: number, membership: MembershipModel) {
    const currentURL = this.API_URL + this.CHATS + chatId + this.MEMBERSHIPS;
    return this.http.post<MembershipModel>(currentURL, membership, this.httpOptions).subscribe(() => {});
  }
}
