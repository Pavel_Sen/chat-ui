import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {UserModel} from '../model/user.model';
import {Injectable} from '@angular/core';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private API_URL = '/chat/api/v1/';
  private USERS = 'users';
  private CURRENT = 'current';

  constructor(private http: HttpClient) {
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  public getCurrentUser(): Observable<UserModel> {
    return this.http.get<UserModel>(this.API_URL + this.USERS + `/${this.CURRENT}`);
  }

  public findAllUsers(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(this.API_URL + this.USERS, this.httpOptions).pipe(
      catchError(this.handleError<UserModel[]>('searchUsersByLogin', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  public registerUser(userLogin: string) {
    const body = {login: userLogin};
    this.http.post(this.API_URL + this.USERS, body).subscribe();
  }

  public findByLogin(login: string) {
    return this.http.get<any>(this.API_URL + this.USERS + `/${login}`);
  }
}
