import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ChatModel} from '../model/chat.model';

@Injectable({
  providedIn: 'root'
})

export class ChatService {
  private API_URL = '/chat/api/v1/';
  private CHATS = 'chats/';
  private USERS = 'users/';

  constructor(private http: HttpClient) {
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  public getChats(): Observable<ChatModel[]> {
    return this.http.get<ChatModel[]>(this.API_URL + this.CHATS, this.httpOptions);
  }

  public findById(id: number): Observable<ChatModel> {
    return this.http.get<ChatModel>(this.API_URL + this.CHATS + id, this.httpOptions);
  }

  public findByUserLogin(login: string): Observable<ChatModel[]> {
    return this.http.get<ChatModel[]>(this.API_URL + this.USERS + login + `/${this.CHATS}`);
  }
}
