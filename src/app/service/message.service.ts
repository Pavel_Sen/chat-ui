import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MessageModel} from '../model/message.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

export interface Message {
  text: string;
}

@Injectable({providedIn: 'root'})
export class MessageService {
  private API_URL = '/chat/api/v1/';
  private CHATS = 'chats/';
  private MESSAGES = '/messages/';

  constructor(private http: HttpClient) {
  }

  public getMessagesByChatId(chatId: number): Observable<MessageModel[]> {
    return this.http.get<MessageModel[]>(this.API_URL + this.CHATS + chatId + this.MESSAGES);
  }

  public sendMessage(chatId: number, message: Message) {
    const currentURL = this.API_URL + this.CHATS + chatId + this.MESSAGES;
    return this.http.post<Message>(currentURL, message, httpOptions).subscribe((success) => console.log(success));
  }
}
