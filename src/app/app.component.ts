import {AfterViewChecked, ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ChatService} from './service/chat.service';
import {ChatModel} from './model/chat.model';
import {UserService} from './service/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {isUndefined} from 'util';
import {MembershipService} from './service/membership.service';
import {MessageService} from './service/message.service';
import {LoaderService} from './service/loader.service';
import {ButtonComponent} from './component/chat/button/button.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {}
}
