import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './component/chat/login/login.component';
import {LoginGuard} from './helper/login-guard.service';
import {AppComponent} from './app.component';
import {HomeComponent} from './component/chat/home/home.component';
import {APP_BASE_HREF, HashLocationStrategy, LocationStrategy} from '@angular/common';


const routes: Routes = [
    {
      path: 'login',
      component: LoginComponent,
      canActivate: [LoginGuard]
    },
    {
      path: '',
      pathMatch: 'full',
      canActivate: [LoginGuard],
      component: AppComponent,
    },
    {
      path: 'home',
      component: HomeComponent,
      canActivate: [LoginGuard]
    }
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: APP_BASE_HREF, useValue: '',
    },
    {
      provide: LocationStrategy, useClass: HashLocationStrategy
    }
  ]
})
export class AppRoutingModule {
}
