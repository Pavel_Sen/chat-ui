import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {ButtonComponent} from './component/chat/button/button.component';
import {HttpClientModule} from '@angular/common/http';
import {fakeBackendProvider} from './mock/service/fake-backend.service';
import {environment} from '../environments/environment';
import {HeaderComponent} from './component/chat/header/header.component';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatRadioModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'hammerjs';
import {MessageFormComponent} from './component/message/form/message-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoaderComponent} from './component/chat/loader/loader.component';
import {ButtonWrapperComponent} from './component/chat/button-wrapper/button-wrapper.component';
import { InfoPanelComponent } from './component/chat/info-panel/info-panel.component';
import {MatDialogModule} from '@angular/material';
import {LoginComponent} from './component/chat/login/login.component';
import {HomeComponent} from './component/chat/home/home.component';
import { MessageComponent } from './component/chat/message/message.component';
import { MessageWrapperComponent } from './component/chat/message-wrapper/message-wrapper.component';

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    MessageFormComponent,
    HeaderComponent,
    LoaderComponent,
    ButtonWrapperComponent,
    InfoPanelComponent,
    LoginComponent,
    HomeComponent,
    MessageComponent,
    MessageWrapperComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    FormsModule,
    MatAutocompleteModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatRadioModule,
    MatButtonModule
  ],
  providers: [
    !environment.production ? fakeBackendProvider : [],
  ],
  entryComponents: [InfoPanelComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
