import {ChatModel} from './chat.model';
import {UserModel} from './user.model';

export class MembershipModel {
  chat: ChatModel;
  user: UserModel;
  userRole: string;
}

