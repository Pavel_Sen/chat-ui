import {UserModel} from './user.model';
import {ChatModel} from './chat.model';

export class MessageModel {
  text: string;

  creation: string;

  user: UserModel;

  chat: ChatModel;

  type: string;
}
