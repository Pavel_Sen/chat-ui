export class ChatModel {
  id: number;
  name: string;
  type: string;
  mode: string;
}
