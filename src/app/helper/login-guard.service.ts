import {Observable} from 'rxjs';
import {UserService} from '../service/user.service';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(private userService: UserService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let result = true;
    console.log(route.queryParams)
    this.userService.getCurrentUser()
      .subscribe(userObservable => {
        this.userService.findByLogin(userObservable.login)
          .subscribe(
            () => {
              this.router.navigate(
                ['/home'], {queryParams: route.queryParams}
              );
              result = true;
            },
            () => {
              this.router.navigate(
                ['/login']
              );
              result = false;
            });
      });
    return result;
  }


}
