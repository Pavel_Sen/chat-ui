import {Component, Inject, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {UserModel} from '../../../model/user.model';
import {UserService} from '../../../service/user.service';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MembershipModel} from '../../../model/membership.model';
import {MembershipService} from '../../../service/membership.service';

export interface DialogData {
  isUserChatAdmin: boolean;
  chatId: number;
}

@Component({
  selector: 'app-info-panel',
  templateUrl: './info-panel.component.html',
  styleUrls: ['./info-panel.component.css']
})
export class InfoPanelComponent implements OnInit {
  users: Observable<UserModel[]>;
  userRoles: string[] = ['ADMIN', 'USER'];
  private addedMemberships: MembershipModel[] = [];
  private availableUsers: UserModel[];
  usersForm: FormGroup;

  constructor(private fb: FormBuilder, private userService: UserService, private dialogRef: MatDialogRef<InfoPanelComponent>,
              private membershipService: MembershipService, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
    this.cleanForms();
    this.userService.getCurrentUser()
      .subscribe(currentUser => {
        this.userService.findAllUsers()
          .subscribe(allUsers => {
            this.availableUsers = allUsers.filter(user => user.login !== currentUser.login);
          });
      });
    this.users = this.usersForm.get('userInput').valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(value => of(this.availableUsers.filter(user => user.login.includes(value)))),
    );
  }

  displayFn(user: UserModel) {
    if (user) {
      return user.login;
    }
  }

  addMembership() {
    const userLoginToAdd = this.usersForm.controls.userInput.value.login;
    const userRole = this.usersForm.controls.userRole.value;
    const user = new UserModel();
    user.login = userLoginToAdd;
    const membership = new MembershipModel();
    membership.user = user;
    membership.userRole = userRole === '' ? 'USER' : userRole;

    this.membershipService.addMembership(this.data.chatId, membership);
    this.addedMemberships.push(membership);
    this.availableUsers = this.availableUsers.filter(u => u.login !== userLoginToAdd);
    this.cleanForms();

  }

  close() {
    this.dialogRef.close(this.addedMemberships);
    this.addedMemberships = [];
  }

  private cleanForms() {
    this.usersForm = this.fb.group({
      userInput: [''],
      userRole: ['']
    });
  }
}
