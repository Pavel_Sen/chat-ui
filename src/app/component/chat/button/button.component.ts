import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from '../../../service/message.service';
import {MessageModel} from '../../../model/message.model';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  public isLoaded = false;

  @Input()
  private chatId: number;

  @Input()
  public chatName: string;

  public lastMessage: MessageModel;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private messageService: MessageService,
              private changerDetector: ChangeDetectorRef) {
  }

  public buttonClick() {
    this.router
      .navigate(['.'], {
        relativeTo: this.activatedRoute,
        queryParams: {
          chat: this.chatId
        },
        queryParamsHandling: 'merge'
      });
  }

  ngOnInit() {
    this.messageService.getMessagesByChatId(this.chatId)
      .subscribe(res => {
          this.lastMessage = res[res.length - 1];
        },
        () => {
        },
        () => {
          this.isLoaded = true;
        }
      );
  }

}
