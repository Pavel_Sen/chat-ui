import {AfterViewChecked, ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ButtonComponent} from '../button/button.component';
import {ChatModel} from '../../../model/chat.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ChatService} from '../../../service/chat.service';
import {UserService} from '../../../service/user.service';
import {MembershipService} from '../../../service/membership.service';
import {MessageService} from '../../../service/message.service';
import {LoaderService} from '../../../service/loader.service';
import {isUndefined} from 'util';
import {UserModel} from '../../../model/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewChecked {

  @ViewChildren('buttonComponent')
  buttons: QueryList<ButtonComponent>;

  title = 'ChatModel-UI';

  chats: Array<ChatModel> = new Array<ChatModel>();

  currentUser: UserModel;

  isRenderChatMessages = false;

  constructor(private route: ActivatedRoute,
              private chatService: ChatService,
              private userService: UserService,
              private membershipService: MembershipService,
              private messageService: MessageService,
              private loaderService: LoaderService,
              private changeDetector: ChangeDetectorRef,
              public router: Router
  ) {
  }

  ngAfterViewChecked(): void {
    const result = this.buttons.map(x => x.isLoaded).find(x => x === false);
    if (!isUndefined(this.buttons.first) && isUndefined(result) && this.loaderService.isLoading.getValue()) {
      console.log('all');
      this.loaderService.hide();
      this.changeDetector.detectChanges();
    }
  }

  ngOnInit() {
    this.loaderService.show();
    this.route.queryParams.subscribe(p => {
      if (!isUndefined(p.chat)) {
        this.chatService.findById(p.chat)
          .subscribe(
            () => {
              this.membershipService.findByChatId(p.chat)
                .subscribe(
                  () => {
                    this.isRenderChatMessages = true;
                  },
                  () => {
                    this.isRenderChatMessages = false;
                  }
                );
            },
            () => {
              this.isRenderChatMessages = false;
            }
          );
      } else {
        this.isRenderChatMessages = false;
      }
    });
    this.userService.getCurrentUser()
      .subscribe(userObserver => {
        this.currentUser = userObserver;
        this.chatService.findByUserLogin(userObserver.login)
          .subscribe(chatObserver => {
              this.chats = chatObserver;
              if (this.chats.length === 0) {
                this.loaderService.hide();
              }
            }
          );
      });
  }
}
