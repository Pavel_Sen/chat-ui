import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {isUndefined} from 'util';
import {MessageService} from '../../../service/message.service';
import {MessageModel} from '../../../model/message.model';
import {UserModel} from '../../../model/user.model';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  public messages: MessageModel[] = [];

  @Input()
  public currentUser: UserModel;

  constructor(private route: ActivatedRoute,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(p => {
      this.messageService.getMessagesByChatId(p.chat)
        .subscribe(success => {
          this.messages = success;
        });
    });
  }

}
