import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ChatService} from '../../../service/chat.service';
import {MembershipModel} from '../../../model/membership.model';
import {MembershipService} from '../../../service/membership.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {InfoPanelComponent} from '../info-panel/info-panel.component';
import {UserService} from '../../../service/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public memberships: MembershipModel[] = [];
  public isCurrentUserAbleToEditChat = false;

  constructor(private route: ActivatedRoute, private chatService: ChatService,
              private membershipService: MembershipService, private userService: UserService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(p => {
      this.membershipService.findByChatId(p.chat).subscribe(res => {
        this.memberships = res;
      });
      this.userService.getCurrentUser().subscribe(user => {
        this.membershipService.findChatMembershipOfCurrentUser(p.chat, user.login).subscribe( res => {
          if (res.chat.type === 'PUBLIC' || (res.chat.type === 'GROUP' && res.userRole === 'ADMIN')) {
            this.isCurrentUserAbleToEditChat = true;
          }
        });
      });
    });
  }

  openDialog() {
    let currentChatId;
    let isCurrentUserChatAdmin = false;
    this.route.queryParams.subscribe(p => {
      currentChatId = p.chat;
      this.userService.getCurrentUser().subscribe(user => {
        this.membershipService.findChatMembershipOfCurrentUser(currentChatId, user.login).subscribe( membership => {
          if (membership.userRole === 'ADMIN') {
            isCurrentUserChatAdmin = true;
          }
          const dialogConfig = new MatDialogConfig();
          dialogConfig.data = {
            chatId: currentChatId,
            isUserChatAdmin: isCurrentUserChatAdmin
          };
          dialogConfig.autoFocus = true;
          dialogConfig.hasBackdrop = false;
          this.dialog.open(InfoPanelComponent, dialogConfig);

          const dialogRef = this.dialog.open(InfoPanelComponent, dialogConfig);

          dialogRef.afterClosed().subscribe(
            data => {
              this.memberships.push(data);
            }
          );
        });
      });
    });
  }
}
