import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../service/user.service';
import {Route, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
  }

  public buttonClick() {
    this.userService.getCurrentUser()
      .subscribe(userObserver => {
        this.userService.registerUser(userObserver.login);
        this.router.navigate(['']);
      });
  }

}
