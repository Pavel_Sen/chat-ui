import {Component, OnInit} from '@angular/core';
import {MessageService} from '../../../service/message.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.css']
})
export class MessageFormComponent implements OnInit {

  text = '';
  chatId: number;

  constructor(private messageService: MessageService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(p => {
      this.chatId = p.chat;
    });
  }

  onSubmit() {
    this.messageService.sendMessage(this.chatId, {text : this.text});
    this.text = '';
  }

}
