import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
// @ts-ignore
import chatsData from '../data/chats.json';
import membershipsData from '../data/memberhips.json';
import userData from '../data/users.json';
import messagesData from '../data/messages.json';
import {delay, dematerialize, materialize, mergeMap} from 'rxjs/operators';
import {ChatModel} from '../../model/chat.model';
import {UserModel} from '../../model/user.model';
import {MembershipModel} from '../../model/membership.model';
import {MessageModel} from '../../model/message.model';

const memberships: MembershipModel[] = membershipsData;
const chats: ChatModel[] = chatsData;
const user: UserModel = userData;
const messages: MessageModel[] = messagesData;

@Injectable()

export class FakeBackend implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const {url, method, headers, body, } = req;

    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());

    function handleRoute() {
      switch (true) {
        case url.endsWith('/users/current') && method === 'GET':
          return getCurrentUser();
        case url.match(/users\/\.*/) && method === 'GET':
          return getUserChats();
        case url.endsWith('/chats') && method === 'GET':
          return getChats();
        case url.match(/.*\/memberships/) && method === 'GET':
          return getMembershipByChatId();
        case url.match(/\/chats\/\d+$/) && method === 'GET':
          return getChatById();
        case url.match(/.*\/messages/) && method === 'GET':
          return getMessages();
        default:
          return next.handle(req);
      }
    }

    function getMessages() {
      return ok(messages);
    }

    function getUserChats() {
      const userMemberships = memberships.filter(x => x.user.login === userLoginFromUrl());
      const userChats = userMemberships.map(x => x.chat);
      return ok(userChats);
    }

    function getMembershipByChatId() {
      const membershipsResult = memberships.filter(x => x.chat.id === getChatIdFromUrl());
      return ok(membershipsResult);
    }

    function getCurrentUser() {
      return ok(user);
    }

    function getChats() {
      return ok(chats);
    }

    function getChatById() {
      const chat = chats.find(x => x.id === idFromUrl());
      return ok(chat);
    }

    function userLoginFromUrl() {
      const urlParts = url.split('/');
      return urlParts[urlParts.length - 3];
    }

    function getChatIdFromUrl() {
      const urlParts = url.split('/');
      return parseInt(urlParts[urlParts.length - 3]);
    }

    function idFromUrl() {
      const urlParts = url.split('/');
      return parseInt(urlParts[urlParts.length - 1]);
    }

    function ok(body?) {
      return of(new HttpResponse({status: 200, body}));
    }
  }
}

export const fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackend,
  multi: true
};
